//
// Created by Damian Stewart on 23/02/2018.
//

#ifndef TRIANGLETRIANGLEINTERSECT_H
#define TRIANGLETRIANGLEINTERSECT_H

#include "Eigen/Dense"

namespace PBD
{
    using Vector3r = Eigen::Matrix<float, 3, 1>;

    bool RayIntersectsTriangle(PBD::Vector3r rayOrigin,
                               PBD::Vector3r rayVector,
                               PBD::Vector3r t0, PBD::Vector3r t1, PBD::Vector3r t2,
                               float *outT)
    {
        const float EPSILON = 0.0000001;
        PBD::Vector3r vertex0 = t0;
        PBD::Vector3r vertex1 = t1;
        PBD::Vector3r vertex2 = t2;
        PBD::Vector3r edge1, edge2, h, s, q;
        float a, f, u, v;
        edge1 = vertex1 - vertex0;
        edge2 = vertex2 - vertex0;
        h = rayVector.cross(edge2);
        a = edge1.dot(h);
        if (a > -EPSILON && a < EPSILON)
            return false;
        f = 1 / a;
        s = rayOrigin - vertex0;
        u = f * (s.dot(h));
        if (u < 0.0 || u > 1.0)
            return false;
        q = s.cross(edge1);
        v = f * rayVector.dot(q);
        if (v < 0.0 || u + v > 1.0)
            return false;
        // At this stage we can compute t to find out where the intersection point is on the line.
        float t = f * edge2.dot(q);
        if (t > EPSILON) // ray intersection
        {
            *outT = t;
            return true;
        } else // This means that there is a line intersection but not a ray intersection.
            return false;
    }

    static bool
    LineSegmentIntersectsTriangle(PBD::Vector3r lineStart, PBD::Vector3r lineEnd, PBD::Vector3r t0, PBD::Vector3r t1, PBD::Vector3r t2)
    {
        auto lineDelta = (lineEnd - lineStart);
        float lineLength = lineDelta.norm();
        PBD::Vector3r rayVector = lineDelta / lineLength;
        float t;
        bool rayIntersects = RayIntersectsTriangle(lineStart, rayVector, t0, t1, t2, &t);
        if (!rayIntersects) {
            return false;
        }
        if (t > lineLength) {
            return false;
        }

        return true;
    }

};

#endif //TRIANGLETRIANGLEINTERSECT_H
