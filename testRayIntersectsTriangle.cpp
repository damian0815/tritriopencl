//
// Created by Damian Stewart on 13/07/2018.
//

#include <iostream>
#include <OpenCL/OpenCL.h>

#define STRINGIFY(...) R"(#__VA_ARGS__)"

const char * RayIntersectsTriangleSourceCL =
#include "RayIntersectsTriangle.cl"
;

#include "TriangleTriangleIntersect.h"
#include "OpenCLContext.h"
#include <limits>

using std::cout;
using std::cerr;
using std::endl;

float randomFloat(float min, float max) {
    auto r = random();
    auto rF = (float)r/(float)RAND_MAX;
    return min + rF * (max-min);
}

PBD::Vector3r randomDirection() {
    auto d = PBD::Vector3r(randomFloat(-1, 1), randomFloat(-1, 1), randomFloat(-1,1));
    return d.normalized();
}

PBD::Vector3r getTriangleNormal(const PBD::Vector3r& t0, const PBD::Vector3r& t1, const PBD::Vector3r& t2)
{
    return (t1-t0).cross(t2-t0).normalized();
}

int main()
{
    OpenCLContext context;
    auto kernel = context.loadProgram(RayIntersectsTriangleSourceCL, "RaysIntersectTriangles");
    if (kernel == NULL) {
        cerr << "Unable to load program" << endl;
        return 1;
    }


    int numValues = 40000;

    cl_float3 rayIn[numValues*2];

    cl_float3 triangleIn[numValues * 3];
    cl_uchar intersectsOut[numValues];
    cl_float tOut[numValues];

    const int kRandomSeed = 100;
    srandom(kRandomSeed);

    for (int i=0; i<numValues; i++) {
        rayIn[i*2+0].x = randomFloat(0, 1);
        rayIn[i*2+0].y = randomFloat(0, 1);
        rayIn[i*2+0].z = randomFloat(0, 1);

        auto d = randomDirection();
        rayIn[i*2+1].x = d[0];
        rayIn[i*2+1].y = d[1];
        rayIn[i*2+1].z = d[2];

        for (int j=0; j<3; j++) {
            triangleIn[i*3+j].x = randomFloat(0,1);
            triangleIn[i*3+j].y = randomFloat(0,1);
            triangleIn[i*3+j].z = randomFloat(0,1);
        }
    }

    cl_mem rayInMem = context.createReadBuffer(numValues * 2 * sizeof(cl_float3));
    cl_mem triangleInMem = context.createReadBuffer(numValues * 3 * sizeof(cl_float3));
    cl_mem intersectsOutMem = context.createWriteBuffer(numValues * sizeof(cl_uchar));
    cl_mem tOutMem = context.createWriteBuffer(numValues * sizeof(cl_float));

    context.setKernelArg_Buffer(0, rayInMem);
    context.setKernelArg_Buffer(1, triangleInMem);
    context.setKernelArg_Buffer(2, intersectsOutMem);
    context.setKernelArg_Buffer(3, tOutMem);

    context.enqueueWriteBuffer(rayInMem, rayIn, numValues * 2 * sizeof(cl_float3));
    context.enqueueWriteBuffer(triangleInMem, triangleIn, numValues * 3 * sizeof(cl_float3));

    const int kBlockSize = 50;
    context.enqueue1DExecute(numValues, kBlockSize);

    context.enqueueReadBuffer(intersectsOut, intersectsOutMem, numValues * sizeof(cl_uchar));
    context.enqueueReadBuffer(tOut, tOutMem, numValues * sizeof(cl_float));

    int failedCount = 0;
    for (int i=0; i<numValues; i++) {

        PBD::Vector3r rayOrigin(rayIn[i*2+0].x, rayIn[i*2+0].y, rayIn[i*2+0].z);
        PBD::Vector3r rayDirection(rayIn[i*2+1].x, rayIn[i*2+1].y, rayIn[i*2+1].z);
        PBD::Vector3r t0(triangleIn[i*3+0].x, triangleIn[i*3+0].y, triangleIn[i*3+0].z);
        PBD::Vector3r t1(triangleIn[i*3+1].x, triangleIn[i*3+1].y, triangleIn[i*3+1].z);
        PBD::Vector3r t2(triangleIn[i*3+2].x, triangleIn[i*3+2].y, triangleIn[i*3+2].z);
        float refT = 0;
        bool refIntersects = PBD::RayIntersectsTriangle(rayOrigin, rayDirection, t0, t1, t2, &refT);

        bool failed = false;
        if (refIntersects != intersectsOut[i]) {
            cerr << "intersect bool mismatch on value " << i << ": " << endl;
            failed = true;
        }
        const float kEpsilon = 1e-5;
        if (intersectsOut[i] && (refT - tOut[i]) > kEpsilon) {
            cerr << "t mismatch on value " << i << ": " << endl;
            failed = true;
        }

        if (failed) {
            ++failedCount;
            cerr << "CL: " << (intersectsOut[i] ? "x" : " ") << " (" << tOut[i] << "), ";
            cerr << "C++: " << (refIntersects ? "x" : " ") << " (" << refT << "), ";
            PBD::Vector3r triangleNormal = getTriangleNormal(t0, t1, t2);
            cerr << "triangle normal vs ray direction: " << triangleNormal.dot(rayDirection) << endl;
        }
    }

    if (failedCount == 0) {
        cout << "All tests passed" << endl;
        return 0;
    } else {
        cerr << failedCount << " tests failed" << endl;
        return 1;
    }

}
