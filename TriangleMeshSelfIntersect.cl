R"(
bool RayIntersectTriangle(float3 rayOrigin, float3 rayDirection, float3 vertex0, float3 vertex1, float3 vertex2, float* tOut);

bool RayIntersectTriangle(float3 rayOrigin, float3 rayDirection, float3 vertex0, float3 vertex1, float3 vertex2, float* tOut)
{
  const float EPSILON = 0.0000001;

  float3 edge1, edge2, h, s, q;
  float a, f, u, v;
  edge1 = vertex1 - vertex0;
  edge2 = vertex2 - vertex0;

  h = cross((float4)(rayDirection, 0), (float4)(edge2, 0)).xyz;
  a = dot(edge1, h);

  if (a > -EPSILON && a < EPSILON) {
    return false;
  }

  f = 1/a;
  s = rayOrigin - vertex0;
  u = f * dot(s, h);
  if (u < 0.0 || u > 1.0) {
    return false;
  }
  q = cross((float4)(s, 0.0), (float4)(edge1, 0.0)).xyz;
  v = f * dot(rayDirection, q);
  if (v < 0.0 || u + v > 1.0) {
    return false;
  }

  // At this stage we can compute t to find out where the intersection point is on the line.
  float t = f * dot(edge2, q);
  if (t > EPSILON) { // ray intersection
    *tOut = t;
    return true;
  } else { // line intersection but no ray intersection.
    return false;
  }
}

__kernel void TriangleMeshSelfIntersect(__global const float3* verticesIn, __global const int* triangleIndices, __global uchar* intersectsOut, __global float* tOut)
{
    const float EPSILON = 0.0000001;

    int triI = get_global_id(0);
    int triJ = get_global_id(1);

    int tI0 = triangleIndices[triI*3+0];
    int tI1 = triangleIndices[triI*3+1];
    int tI2 = triangleIndices[triI*3+2];
    int tJ0 = triangleIndices[triJ*3+0];
    int tJ1 = triangleIndices[triJ*3+1];
    int tJ2 = triangleIndices[triJ*3+2];

    float3 vI0 = verticesIn[tI0];
    float3 vI1 = verticesIn[tI1];
    float3 vI2 = verticesIn[tI2];

    float3 vJ0 = verticesIn[tJ0];
    float3 vJ1 = verticesIn[tJ1];
    float3 vJ2 = verticesIn[tJ2];

    int outIdxOffset = 3 * (get_global_id(0)*get_global_size(0) + get_global_id(1));

    float t;
    intersectsOut[outIdxOffset+0] = RayIntersectTriangle(vJ0, normalize(vJ1-vJ0), vI0, vI1, vI2, &t);
    tOut[outIdxOffset+0] = t;
    intersectsOut[outIdxOffset+1] = RayIntersectTriangle(vJ1, normalize(vJ2-vJ1), vI0, vI1, vI2, &t);
    tOut[outIdxOffset+1] = t;
    intersectsOut[outIdxOffset+2] = RayIntersectTriangle(vJ2, normalize(vJ0-vJ2), vI0, vI1, vI2, &t);
    tOut[outIdxOffset+2] = t;
}



)";
