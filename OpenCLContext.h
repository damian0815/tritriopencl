//
// Created by Damian Stewart on 12.07.18.
//

#ifndef TRITRIOPENCL_OPENCLCONTEXT_H
#define TRITRIOPENCL_OPENCLCONTEXT_H

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#include <vector>


class OpenCLContext
{
public:
    OpenCLContext();
    virtual ~OpenCLContext();

    void logDeviceInfo();

    cl_kernel loadProgram(const char *program, const char *kernelName);

    cl_mem createReadBuffer(size_t byteSize);
    cl_mem createWriteBuffer(size_t byteSize);

    void enqueueReadBuffer(void *target, cl_mem source, size_t byteSize);
    void enqueueWriteBuffer(cl_mem target, void *source, size_t byteSize);

    void setKernelArg_Buffer(cl_uint argIndex, cl_mem buffer);

    bool enqueue1DExecute(size_t globalSize, size_t blockSize);
    bool enqueue2DExecute(size_t globalSize, size_t blockSize);

    void flush();
    void finish();
    void blockUntilDone() { finish(); }

private:

    cl_mem createBuffer(cl_mem_flags flags, size_t byteSize);

    cl_context mContext = NULL;
    cl_command_queue mCommandQueue = NULL;
    cl_device_id mDeviceId = NULL;

    cl_program mProgram = NULL;
    cl_kernel mKernel = NULL;

    std::vector<cl_mem> mBuffers;

};


#endif //TRITRIOPENCL_OPENCLCONTEXT_H
