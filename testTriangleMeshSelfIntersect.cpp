//
// Created by Damian Stewart on 13/07/2018.
//

//
// Created by Damian Stewart on 13/07/2018.
//

#include <iostream>
#include <OpenCL/OpenCL.h>

#define STRINGIFY(...) R"(#__VA_ARGS__)"

const char * TriangleMeshSelfIntersect =
#include "TriangleMeshSelfIntersect.cl"

bool
checkAndLogFailure(bool refIntersects, float refT, const cl_uchar *intersectsOut, const cl_float *tOut, int triangleI, int triangleJ, int clIntersectsIndexOffset, const int edgeIndex);


#include "TriangleTriangleIntersect.h"
#include "OpenCLContext.h"
#include <limits>

using std::cout;
using std::cerr;
using std::endl;

float randomFloat(float min, float max) {
    auto r = random();
    auto rF = (float)r/(float)RAND_MAX;
    return min + rF * (max-min);
}

PBD::Vector3r getRayDirection(cl_float3 to, cl_float3 from);

PBD::Vector3r toV3(cl_float3 v);

PBD::Vector3r getTriangleNormal(const PBD::Vector3r& t0, const PBD::Vector3r& t1, const PBD::Vector3r& t2)
{
    return (t1-t0).cross(t2-t0).normalized();
}

int main()
{
    OpenCLContext context;
    context.logDeviceInfo();

    auto kernel = context.loadProgram(TriangleMeshSelfIntersect, "TriangleMeshSelfIntersect");
    if (kernel == NULL) {
        cerr << "Unable to load program" << endl;
        return 1;
    }


    const int kRandomSeed = 100;
    srandom(kRandomSeed);



    const int numTriangles = 200;
    cl_float3 verticesIn[numTriangles*3];
    cl_int trianglesIn[numTriangles*3];

    for (int i=0; i<numTriangles*3; i++) {
        trianglesIn[i] = i;

        verticesIn[i].x = randomFloat(0, 1);
        verticesIn[i].y = randomFloat(0, 1);
        verticesIn[i].z = randomFloat(0, 1);

    }

    const int numPotentialIntersections = numTriangles * numTriangles * 3;
    cl_uchar intersectsOut[numPotentialIntersections];
    cl_float tOut[numPotentialIntersections];


    cl_mem verticesInMem = context.createReadBuffer(numTriangles * 3 * sizeof(cl_float3));
    cl_mem trianglesInMem = context.createReadBuffer(numTriangles * 3 * sizeof(cl_int));
    cl_mem intersectsOutMem = context.createWriteBuffer(numPotentialIntersections * sizeof(cl_uchar));
    cl_mem tOutMem = context.createWriteBuffer(numPotentialIntersections * sizeof(cl_float));

    context.setKernelArg_Buffer(0, verticesInMem);
    context.setKernelArg_Buffer(1, trianglesInMem);
    context.setKernelArg_Buffer(2, intersectsOutMem);
    context.setKernelArg_Buffer(3, tOutMem);

    context.enqueueWriteBuffer(verticesInMem, verticesIn, numTriangles * 3 * sizeof(cl_float3));
    context.enqueueWriteBuffer(trianglesInMem, trianglesIn, numTriangles * 3 * sizeof(cl_int));

    const int kBlockSize = 20;
    bool enqueued = context.enqueue2DExecute(numTriangles, kBlockSize);
    if (!enqueued) {
        cerr << "could not execute" << endl;
        return 1;
    }


    context.enqueueReadBuffer(intersectsOut, intersectsOutMem, numPotentialIntersections * sizeof(cl_uchar));
    context.enqueueReadBuffer(tOut, tOutMem, numPotentialIntersections * sizeof(cl_float));

    int failedCount = 0;
    for (int triangleI=0; triangleI<numTriangles; ++triangleI) {

        auto tI0 = trianglesIn[triangleI*3+0];
        auto tI1 = trianglesIn[triangleI*3+1];
        auto tI2 = trianglesIn[triangleI*3+2];
        auto vI0 = verticesIn[tI0];
        auto vI1 = verticesIn[tI1];
        auto vI2 = verticesIn[tI2];

        for (int triangleJ = 0; triangleJ < numTriangles; ++triangleJ) {
            auto tJ0 = trianglesIn[triangleJ*3+0];
            auto tJ1 = trianglesIn[triangleJ*3+1];
            auto tJ2 = trianglesIn[triangleJ*3+2];
            auto vJ0 = verticesIn[tJ0];
            auto vJ1 = verticesIn[tJ1];
            auto vJ2 = verticesIn[tJ2];

            float refT = 0;
            bool refIntersects;
            int clIntersectsIndexOffset = (triangleI*numTriangles + triangleJ) * 3;

            refIntersects = PBD::RayIntersectsTriangle(toV3(vJ0), getRayDirection(vJ1, vJ0), toV3(vI0), toV3(vI1), toV3(vI2), &refT);
            if (checkAndLogFailure(refIntersects, refT, intersectsOut, tOut, triangleI, triangleJ, clIntersectsIndexOffset, 0)) {
                cerr << "CL: " << (intersectsOut[clIntersectsIndexOffset+0] ? "x" : " ") << " (" << tOut[clIntersectsIndexOffset+0] << "), ";
                cerr << "C++: " << (refIntersects ? "x" : " ") << " (" << refT << ")" << endl;
                ++failedCount;
            } /*else {
                cout << "CL: " << (intersectsOut[clIntersectsIndexOffset+0] ? "x" : " ") << " (" << tOut[clIntersectsIndexOffset+0] << "), ";
                cout << "C++: " << (refIntersects ? "x" : " ") << " (" << refT << ")" << endl;
            }*/

            refIntersects = PBD::RayIntersectsTriangle(toV3(vJ1), getRayDirection(vJ2, vJ1), toV3(vI0), toV3(vI1), toV3(vI2), &refT);
            if (checkAndLogFailure(refIntersects, refT, intersectsOut, tOut, triangleI, triangleJ, clIntersectsIndexOffset, 1)) {
                cerr << "CL: " << (intersectsOut[clIntersectsIndexOffset+1] ? "x" : " ") << " (" << tOut[clIntersectsIndexOffset+1] << "), ";
                cerr << "C++: " << (refIntersects ? "x" : " ") << " (" << refT << ")" << endl;
                ++failedCount;
            } /*else {
                cout << "CL: " << (intersectsOut[clIntersectsIndexOffset+1] ? "x" : " ") << " (" << tOut[clIntersectsIndexOffset+1] << "), ";
                cout << "C++: " << (refIntersects ? "x" : " ") << " (" << refT << ")" << endl;
            }*/

            refIntersects = PBD::RayIntersectsTriangle(toV3(vJ2), getRayDirection(vJ0, vJ2), toV3(vI0), toV3(vI1), toV3(vI2), &refT);
            if (checkAndLogFailure(refIntersects, refT, intersectsOut, tOut, triangleI, triangleJ, clIntersectsIndexOffset, 2)) {
                cerr << "CL: " << (intersectsOut[clIntersectsIndexOffset+2] ? "x" : " ") << " (" << tOut[clIntersectsIndexOffset+2] << "), ";
                cerr << "C++: " << (refIntersects ? "x" : " ") << " (" << refT << ")" << endl;
                ++failedCount;
            }

        }
    }

    if (failedCount == 0) {
        cout << "All tests passed" << endl;
        return 0;
    } else {
        cerr << failedCount << " tests failed out of " << numPotentialIntersections << endl;
        return 1;
    }

}

PBD::Vector3r getRayDirection(cl_float3 to, cl_float3 from)
{
    return (PBD::Vector3r(to.x, to.y, to.z) - PBD::Vector3r(from.x, from.y, from.z)).normalized();
}

PBD::Vector3r toV3(cl_float3 v)
{
    return PBD::Vector3r(v.x, v.y, v.z);
}

bool
checkAndLogFailure(bool refIntersects, float refT, const cl_uchar *intersectsOut, const cl_float *tOut, int triangleI, int triangleJ, int clIntersectsIndexOffset, const int edgeIndex)
{
    bool failed = false;
    if (refIntersects != intersectsOut[clIntersectsIndexOffset + edgeIndex]) {
        cerr << "intersect bool mismatch on " << triangleI << " vs " << triangleJ << ":" << edgeIndex << endl;
        failed = true;
    }
    const float kEpsilon = 1e-5;
    if (intersectsOut[clIntersectsIndexOffset + edgeIndex] && (refT - tOut[clIntersectsIndexOffset + edgeIndex]) > kEpsilon) {
        cerr << "t mismatch on  " << triangleI << " vs " << triangleJ << ":" << edgeIndex << endl;
        failed = true;
    }
    return failed;
}
