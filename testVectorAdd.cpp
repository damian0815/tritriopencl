#include <iostream>
#include <OpenCL/OpenCL.h>

#define STRINGIFY(...) #__VA_ARGS__

const char * VectorAddSourceCL =
#include "VectorAdd.cl"
;

#include "TriangleTriangleIntersect.h"
#include "OpenCLContext.h"

using std::cout;
using std::endl;
using std::cerr;

int main()
{
    OpenCLContext context;
    auto kernel = context.loadProgram(VectorAddSourceCL, "VectorAdd");

    if (kernel == NULL) {
        cerr << "Unable to load program" << endl;
        return 1;
    }

    int numValues = 1000;

    int a[numValues];
    int b[numValues];
    for (int i=0; i< numValues; i++) {
        a[i] = i;
        b[i] = i;
    }
    int result[numValues];

    cl_mem aMem = context.createReadBuffer(numValues * sizeof(int));
    cl_mem bMem = context.createReadBuffer(numValues * sizeof(int));
    cl_mem resultMem = context.createWriteBuffer(numValues * sizeof(int));

    context.setKernelArg_Buffer(0, aMem);
    context.setKernelArg_Buffer(1, bMem);
    context.setKernelArg_Buffer(2, resultMem);

    context.enqueueWriteBuffer(aMem, a, numValues * sizeof(int));
    context.enqueueWriteBuffer(bMem, b, numValues * sizeof(int));

    const int kBlockSize = 50;
    context.enqueue1DExecute(numValues, kBlockSize);

    context.enqueueReadBuffer(result, resultMem, numValues * sizeof(int));

    for (int i=0; i<numValues; i++) {
        cout << a[i] << " + " << b[i] << " = " << result[i] << endl;
    }

    return 0;
}
