//
// Created by Damian Stewart on 12.07.18.
//

#include <cstring>
#include <iostream>
#include "OpenCLContext.h"

using std::cerr;
using std::cout;
using std::endl;


OpenCLContext::OpenCLContext()
{
    // Get platform and device information
    cl_platform_id platform_id = NULL;
    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;
    cl_int ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
    if (ret != CL_SUCCESS) {
        throw std::runtime_error("could not get platform id");
    }

    ret = clGetDeviceIDs( platform_id, CL_DEVICE_TYPE_DEFAULT, 1,
                          &mDeviceId, &ret_num_devices);
    if (ret != CL_SUCCESS) {
        throw std::runtime_error("could not get device ids");
    }

    // Create an OpenCL context
    mContext = clCreateContext( NULL, 1, &mDeviceId, NULL, NULL, &ret);
    if (ret != CL_SUCCESS) {
        throw std::runtime_error("could not create context");
    }

    // Create a command queue
    mCommandQueue = clCreateCommandQueue(mContext, mDeviceId, 0, &ret);
    if (ret != CL_SUCCESS) {
        throw std::runtime_error("could not create command queue");
    }
}

cl_kernel OpenCLContext::loadProgram(const char *program, const char *kernelName)
{
    cl_int result;

    const auto programSize = strlen(program);
    mProgram = clCreateProgramWithSource(mContext, 1,
                                                   (const char **)&program, (const size_t *)&programSize, &result);
    if (result != CL_SUCCESS) {
        cerr << "error creating program: " << result << endl;
        return NULL;
    }

    result = clBuildProgram(mProgram, 1, &mDeviceId, NULL, NULL, NULL);
    if (result != CL_SUCCESS) {
        cerr << "error building program: " << result << endl;
        char buildLog[16384];
        size_t buildLogSize;
        clGetProgramBuildInfo(mProgram, mDeviceId, CL_PROGRAM_BUILD_LOG, 16384, buildLog, &buildLogSize);
        cerr << buildLog << endl;
        return NULL;
    }

    mKernel = clCreateKernel(mProgram, kernelName, &result);
    if (result != CL_SUCCESS) {
        cerr << "error creating kernel: " << result << endl;
        return NULL;
    }

    return mKernel;
}


OpenCLContext::~OpenCLContext()
{
    if (mKernel != NULL) {
        clReleaseKernel(mKernel);
    }
    if (mProgram != NULL) {
        clReleaseProgram(mProgram);
    }

    for (auto& m: mBuffers) {
        clReleaseMemObject(m);
    }
    mBuffers.clear();

    clReleaseCommandQueue(mCommandQueue);
    clReleaseContext(mContext);
}

void OpenCLContext::logDeviceInfo()
{
    size_t actualSize;
    char stringBuffer[1024];

    clGetDeviceInfo(mDeviceId, CL_DEVICE_NAME, 1024, stringBuffer, &actualSize);
    cout << "Device name: " << stringBuffer << endl;

    clGetDeviceInfo(mDeviceId, CL_DEVICE_PROFILE, 1024, stringBuffer, &actualSize);
    cout << "Device profile: " << stringBuffer << endl;

    size_t maxWorkGroupSize;
    clGetDeviceInfo(mDeviceId, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(maxWorkGroupSize), &maxWorkGroupSize, &actualSize);
    cout << "Max work group size: " << maxWorkGroupSize << endl;

}

cl_mem OpenCLContext::createReadBuffer(size_t byteSize)
{
    return createBuffer(CL_MEM_READ_ONLY, byteSize);
}

cl_mem OpenCLContext::createWriteBuffer(size_t byteSize)
{
    return createBuffer(CL_MEM_WRITE_ONLY, byteSize);
}

cl_mem OpenCLContext::createBuffer(cl_mem_flags flags, size_t byteSize)
{
    cl_int ret;
    auto buffer = clCreateBuffer(mContext, flags, byteSize, NULL, &ret);
    if (ret != CL_SUCCESS) {
        cerr << "cannot create buffer with size " << byteSize << " bytes: "
             << ret << endl;
        return NULL;
    }

    mBuffers.push_back(buffer);
    return buffer;
}

void
OpenCLContext::enqueueReadBuffer(void *target, cl_mem source, size_t byteSize)
{
    const auto kBlocking = CL_TRUE;
    const auto kOffset = 0;
    auto ret = clEnqueueReadBuffer(mCommandQueue, source, kBlocking, kOffset, byteSize, target, 0, NULL, NULL);
    if (ret != CL_SUCCESS) {
        cerr << "could not enqueue read buffer: " << ret << endl;
    }
}

void
OpenCLContext::enqueueWriteBuffer(cl_mem target, void *source, size_t byteSize)
{
    const auto kBlocking = CL_TRUE;
    const auto kOffset = 0;
    auto ret = clEnqueueWriteBuffer(mCommandQueue, target, kBlocking, kOffset, byteSize, source, 0, NULL, NULL);
    if (ret != CL_SUCCESS) {
        cerr << "could not enqueue read buffer: " << ret << endl;
    }
}


void OpenCLContext::setKernelArg_Buffer(cl_uint argIndex, cl_mem buffer)
{
    auto ret = clSetKernelArg(mKernel, argIndex, sizeof(cl_mem), (void*)&buffer);
    if (ret != CL_SUCCESS) {
        cerr << "could net set kernel arg: " << ret << endl;
    }
}

bool OpenCLContext::enqueue1DExecute(size_t globalSize, size_t blockSize)
{
    if ((globalSize % blockSize) != 0) {
        throw std::runtime_error("globalSize must be a multiple of blockSize");
    }
    const cl_uint kWorkDim = 1;
    auto ret = clEnqueueNDRangeKernel(mCommandQueue, mKernel, kWorkDim, NULL, &globalSize, &blockSize, 0, NULL, NULL);
    if (ret == CL_SUCCESS) {
        return true;
    }

    cerr << "could not enequeue execute: " << ret << endl;
    return false;
}

bool OpenCLContext::enqueue2DExecute(size_t globalSize, size_t blockSize)
{
    if ((globalSize % blockSize) != 0) {
        throw std::runtime_error("globalSize must be a multiple of blockSize");
    }
    const cl_uint kWorkDim = 2;
    size_t globalSizes[2] = { globalSize, globalSize };
    size_t blockSizes[2] = { blockSize, blockSize };
    auto ret = clEnqueueNDRangeKernel(mCommandQueue, mKernel, kWorkDim, NULL, globalSizes, blockSizes, 0, NULL, NULL);
    if (ret == CL_SUCCESS) {
        return true;
    }

    cerr << "could not enequeue execute: " << ret << endl;
    return false;
}

void OpenCLContext::flush()
{
    clFlush(mCommandQueue);
}

void OpenCLContext::finish()
{
    clFinish(mCommandQueue);
}
