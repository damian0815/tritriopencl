
R"(
bool RayIntersectTriangle(float3 rayOrigin, float3 rayDirection, float3 vertex0, float3 vertex1, float3 vertex2, float* tOut);

bool RayIntersectTriangle(float3 rayOrigin, float3 rayDirection, float3 vertex0, float3 vertex1, float3 vertex2, float* tOut)
{
  const float EPSILON = 0.0000001;

  float3 edge1, edge2, h, s, q;
  float a, f, u, v;
  edge1 = vertex1 - vertex0;
  edge2 = vertex2 - vertex0;

  h = cross((float4)(rayDirection, 0), (float4)(edge2, 0)).xyz;
  a = dot(edge1, h);

  if (a > -EPSILON && a < EPSILON) {
    return false;
  }

  f = 1/a;
  s = rayOrigin - vertex0;
  u = f * dot(s, h);
  if (u < 0.0 || u > 1.0) {
    return false;
  }
  q = cross((float4)(s, 0.0), (float4)(edge1, 0.0)).xyz;
  v = f * dot(rayDirection, q);
  if (v < 0.0 || u + v > 1.0) {
    return false;
  }

  // At this stage we can compute t to find out where the intersection point is on the line.
  float t = f * dot(edge2, q);
  if (t > EPSILON) { // ray intersection
    *tOut = t;
    return true;
  } else { // line intersection but no ray intersection.
    return false;
  }
}

__kernel void RaysIntersectTriangles(__global const float3* rayIn, __global const float3* triangleIn, __global uchar* intersectsOut, __global float* tOut)
{
    const float EPSILON = 0.0000001;

    int i = get_global_id(0);
    float3 rayOrigin = rayIn[i*2+0];
    float3 rayVector = rayIn[i*2+1];

    float3 vertex0 = triangleIn[i*3+0];
    float3 vertex1 = triangleIn[i*3+1];
    float3 vertex2 = triangleIn[i*3+2];

    float t;
    intersectsOut[i] = RayIntersectTriangle(rayOrigin, rayVector, vertex0, vertex1, vertex2, &t);
    tOut[i] = t;
}



)";
